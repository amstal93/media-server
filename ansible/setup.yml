---
- hosts: all
  remote_user: root
  gather_facts: true
  vars:
    create_user: "{{ lookup('env', 'USER') }}"
  tasks:

    - name: Make sure we have a 'wheel' group
      ansible.builtin.group:
        name: wheel
        state: present

    - name: Allow 'wheel' group to have passwordless sudo
      ansible.builtin.lineinfile:
        path: /etc/sudoers
        state: present
        regexp: '^%wheel'
        line: '%wheel ALL=(ALL) NOPASSWD: ALL'
        validate: '/usr/sbin/visudo -cf %s'

    - name: Disable password authentication for root
      ansible.builtin.lineinfile:
        path: /etc/ssh/sshd_config
        state: present
        regexp: '^#?PermitRootLogin'
        line: 'PermitRootLogin prohibit-password'

    - name: Enable password authentication for non-root
      ansible.builtin.lineinfile:
        path: /etc/ssh/sshd_config
        state: present
        regexp: '^#?PasswordAuthentication'
        line: 'PasswordAuthentication yes'

    - name: Restart ssh service
      ansible.builtin.service:
        name: ssh
        state: restarted

    - name: Install required system packages
      ansible.builtin.apt:
        name: "{{ packages }}"
        state: latest
        update_cache: yes
      vars:
        packages:
          - fuse
          - python3-pip

    - name: Install Docker pip packages
      ansible.builtin.pip:
        name: "{{ packages }}"
      vars:
        packages:
          - docker
          - docker-compose


    - name: Create a new regular user with sudo privileges
      ansible.builtin.user:
        name: "{{ create_user }}"
        state: present
        groups:
          - docker
          - wheel
        append: true
        create_home: true
        shell: /bin/bash
        password: "{{ lookup('env', 'PASSWORD') | password_hash('sha512') }}"

    - name: Set authorized key for remote user
      ansible.posix.authorized_key:
        user: "{{ create_user }}"
        state: present
        key: "{{ lookup('file', '~/.ssh/id_rsa.pub') }}"
